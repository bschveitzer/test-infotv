import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import routes from './routes';
import axios from 'axios';

Vue.use(VueRouter);

/**
 * @author Bernardo Schveitzer
 * Configurações do roteador.
 * @type {VueRouter}
 */
const router = new VueRouter({
  routes: routes,
  linkActiveClass: "active-page",
  linkExactActiveClass: "current-page"
});

/**
 * @author Bernardo Schveitzer
 * Configuração da URL base da API.
 * @type {axios}
 */
axios.defaults.baseURL = 'http://front-test.diga.net.br/api';

/**
 * Inicialização do Vue passando o que deve ser injetado.
 * (router: VueRouter, store: VueX)
 */
new Vue({
  router,
  render: h => h(App),
}).$mount('#container');
